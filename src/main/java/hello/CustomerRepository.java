package hello;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import hello.server.Cards;
import hello.server.Customer;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class CustomerRepository {
	private static final Map<Integer, Customer> countries = new HashMap<>();

	@PostConstruct
	public void initData() {
		Customer spain = new Customer();
		spain.setCustomerid(1);
		spain.setVatnumber("555555555K");
		spain.setPmastore("Alcorcon");
		spain.setNationality("ES");
		spain.setFirstname("Firstname");
		spain.setLastname1("Lastanme");
		spain.setLastname2("Lastanme");
		spain.setGender("Male");

//		Cards cards = new Cards(){{setCard(new List<String>() {{}});}}
//		cards.add("1234567890");
//		spain.setCards(( Cards ) cards);


		countries.put(spain.getCustomerid(), spain);

//		Country poland = new Country();
//		poland.setName("Poland");
//		poland.setCapital("Warsaw");
//		poland.setPopulation(38186860);
//
//		countries.put(poland.getName(), poland);
//
//		Country uk = new Country();
//		uk.setName("United Kingdom");
//		uk.setCapital("London");
//		uk.setPopulation(63705000);
//
//		countries.put(uk.getName(), uk);
	}

	public Customer findCustomer(Integer customerid) {
		Assert.notNull(customerid, "The country's name must not be null");
		return countries.get(customerid);
	}
}
